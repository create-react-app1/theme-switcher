import React from 'react'
import './App.css'

// Services
import ThemeContextProvider from './services/contexts/theme'

// Components
import StyleComponentContainer from './components/style-component'
import CSSComponentContainer from './components/css-component'
import StyledCSSComponentContainer from './components/styled-css-component'

function App() {
  return (
    <div className="App">
      <header className="App-header"><h1>Theme Switcher</h1></header>

      <div className="styled-container">
        <ThemeContextProvider>
          <StyleComponentContainer />
        </ThemeContextProvider>
      </div>

      <div className="container css-container">
        <CSSComponentContainer />
      </div>

      {/* <div className="styled-css-container">
        <StyledCSSComponentContainer />
      </div> */}
    </div>
  )
}

export default App
