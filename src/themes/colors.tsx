/**
  Colors

  Obs: Mostly of the color variables names are from aproximates names.
  Reference: https://www.htmlcsscolor.com
  
**/
const colors = {
  amaranth: "#E23E57",
  black: "#000000",
  blackCurrant: "#1A1423",
  charcoal: "#444444",
  darkGoldenrod: "#BF9000",
  freeSpeechRed: "#CC0000",
  lime: "#00FF00",
  persianIndigo: "#351C75",
  white: "#FFFFFF",
  whiteSmoke: "#F3F3F3",
};

export interface AllThemesColorsProps {
  default: { [color: string]: string };
  main: { [color: string]: string };
  dark: { [color: string]: string };
  [theme: string]: { [color: string]: string };
}

const allThemesColors = {
  default: {
    error: colors.freeSpeechRed,
    error_opacity: colors.freeSpeechRed + "aa",
    warn: colors.darkGoldenrod,
    info: colors.persianIndigo,
    success: colors.lime,
  },
  main: {
    primary_00: colors.black,
    secondary_00: colors.whiteSmoke,

    primaryBG_00: colors.whiteSmoke,
  },
  dark: {
    primary_00: colors.white,
    secondary_00: colors.black,

    primaryBG_00: colors.black,
  },
  vampire: {
    primary_00: colors.amaranth,
    secondary_00: colors.black,

    primaryBG_00: colors.blackCurrant,

    info: colors.whiteSmoke,
  },
} as AllThemesColorsProps;

export default allThemesColors;

export const getThemesName = () =>
  Object.keys(allThemesColors).filter((t) => t !== "default");

export const getColor = (kindColor: string, themeName?: string) => {
  const colorByTheme =
    themeName &&
    allThemesColors[themeName] &&
    allThemesColors[themeName][kindColor];
  return colorByTheme || allThemesColors["default"][kindColor];
};
