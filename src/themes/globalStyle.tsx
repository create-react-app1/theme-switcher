import { createGlobalStyle } from 'styled-components'

import theme from './colors'

const createRootStyles = () => {
  const themeKeys = Object.keys(theme)

  return themeKeys.map(tk => {

    const root = ":root" + (tk === "default" ? "" : "." + tk)

    const themeColorsKeys = Object.keys(theme[tk])

    const stringColorsOfTheme = themeColorsKeys.map(tck => "--"+tck+":"+theme[tk][tck]+";").join("\n")

    return [root, "{", stringColorsOfTheme, "}"].join("\n")

  }).join("\n")

}

const GlobalStyle = createGlobalStyle`

  ${ createRootStyles() }

`

export default GlobalStyle