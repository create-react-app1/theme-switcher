import styled from 'styled-components'

export default styled.div`

  display: inline-block;
  height: 100%;

  background-color: ${props => props.theme.colors.primaryBG_00};
  color: ${props => props.theme.colors.primary_00};

  .btn-info {
    background-color: ${props => props.theme.getColor('info')};
  }

  .btn-warn {
    background-color: ${props => props.theme.getColor('warn')};
  }

  .btn-error {
    background-color: ${props => props.theme.getColor('error')};
  }

  .btn-success {
    background-color: ${props => props.theme.getColor('success')};
  }

  .btn-custom {
    background-color: ${props => props.theme.getColor('primary_00')};
    color: ${props => props.theme.colors.secondary_00};
  }

  .theme-references .color-references span {
    border-color: ${props => props.theme.getColor('primary_00')};
  }

`