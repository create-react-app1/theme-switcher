import React, { useContext } from 'react'

// Services
import { ThemeContext } from '../../services/contexts/theme'

// Components
import Content from '../content'

// Style Components
import { ThemeProvider } from "styled-components"
import Style from './style'

// Colors
import allThemesColors, { getThemesName, getColor } from '../../themes/colors'
const themesName = getThemesName() 

const StyleContentContainer: React.FC = () => {

  const context = useContext(ThemeContext)
  const theme = context.theme || "main"

  return (
    <ThemeProvider 
      theme={{
        colors: allThemesColors[theme], 
        getColor: (colorName: string) => getColor(colorName, theme)
      }}>
    <Style className="container">
      <Content
        title="Style Components"
        subtitle="Switcher with API Context"
        theme={theme}
        themeColors={allThemesColors[theme] || {}}
        allThemesName={themesName}
        onChangeTheme={( (theme: string) => context.setTheme && context.setTheme(theme) )} 
      />
    </Style>
    </ThemeProvider>
  )
}

export default StyleContentContainer