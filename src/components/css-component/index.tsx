import React, { useState, useEffect } from "react";

// Components
import Content from "../content";

// Style Components
import "../../themes/colors.css";
import "./style.css";

const CSSContentContainer: React.FC = () => {
  const [theme, setTheme] = useState("main");
  const [allThemeColors, setAllThemeColors] = useState(
    {} as { [color: string]: string }
  );

  useEffect(() => {
    themeSwitcher(theme);
    setAllThemeColors(getAllThemeColors());
  }, [theme]);

  const themeSwitcher = (theme: string) => {
    const rootElement = document.querySelector(":root");
    rootElement && rootElement.setAttribute("class", theme);
  };

  const getAllThemeColors = () => {
    const rootElement = document.querySelector(":root");
    let colorObject = {} as { [color: string]: string };

    ["--primary-00", "--secondary-00", "--primaryBG-00"].forEach(
      (color: string) =>
        (colorObject[color] = getComputedStyle(rootElement as Element)
          .getPropertyValue(color)
          .trim())
    );

    return colorObject;
  };

  return (
    <Content
      title="CSS Var"
      subtitle="Switcher with State"
      allThemesName={["main", "dark", "summer", "winter"]}
      theme={theme}
      themeColors={allThemeColors}
      onChangeTheme={(newTheme: string) => setTheme(newTheme)}
    />
  );
};

export default CSSContentContainer;
