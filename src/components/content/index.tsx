import React from 'react'

export interface ContentProps {
  title?: string;
  subtitle?: string; 

  theme: string;
  themeColors: { [color:string]: string };
  allThemesName: Array<string>;
  onChangeTheme: Function
}

const Content: React.FC<ContentProps> = ({title, subtitle, theme, themeColors, allThemesName, onChangeTheme}) => {

  const createSelectOptions = () => 
  allThemesName.map((t, i) => <option key={i} value={t}>{t}</option>)

  
  const createColorsReferencesOfTheme = () => {

    const keys = Object.keys(themeColors)

    return keys.map(
      (k, i) => 
      <div className="color-references" key={i}>
        {k}: {themeColors[k]}
        <span style={{backgroundColor: themeColors[k]}} />
      </div>
    )
  }

  return (
    <>
      <h3>{title}</h3>
      <h4>{subtitle}</h4>

      <select 
        value={theme} 
        onChange={(e) => onChangeTheme(e.target.value)}
      >{createSelectOptions()}</select>

      <div className="theme-references">{createColorsReferencesOfTheme()}</div>

      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis semper orci vitae massa volutpat sodales a sit amet elit. Cras augue nisl, bibendum sed efficitur at, accumsan dictum lectus. Donec lacinia eros ac magna malesuada ultrices. Aenean lobortis maximus massa, quis mollis felis venenatis et. In luctus augue lacus.</p>

      <button className="btn btn-info">Info</button>
      <button className="btn btn-warn">Warn</button>
      <button className="btn btn-error">Error</button>
      <button className="btn btn-success">Success</button>
      <button className="btn btn-custom">Custom</button>
    </>
  )
}

export default Content