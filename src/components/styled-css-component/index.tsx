import React, { useState, useEffect } from "react"

// Components
import Content from "../content"

// Style Components
import GlobalStyle from '../../themes/globalStyle'
import Style from './style'

// Colors
import allThemesColors, { getThemesName } from '../../themes/colors'
const themesName = getThemesName() 

const CSSContentContainer: React.FC = () => {
  const [theme, setTheme] = useState("main")

  useEffect(() => {
    themeSwitcher(theme)
  }, [theme])

  const themeSwitcher = (theme: string) => {
    const rootElement = document.querySelector(":root")
    rootElement && rootElement.setAttribute("class", theme)
  }

  return (
    <Style theme={theme}>
    <GlobalStyle />
    <Content
      title="Styled and CSS Var"
      subtitle="Working together"
      theme={theme}
      themeColors={allThemesColors[theme] || {}}
      allThemesName={themesName}
      onChangeTheme={(newTheme: string) => setTheme(newTheme)}
    />
    </Style>
  )
}

export default CSSContentContainer
