import styled from 'styled-components'
import { getColor } from '../../themes/colors'

export default styled.div`

  display: inline-block;
  height: 100%;

  background-color: var(--primaryBG_00);
  color: var(--primary_00);

  text-align: center;
  padding: 20px;
  height: 500px !important;

  .btn-info {
    background-color: var(--info);
  }

  .btn-warn {
    background-color: var(--warn);
  }

  .btn-error {
    background-color: var(--error);
    background-color: ${props => getColor('error', props.theme)}aa;
    background-color var(--error_opacity);
  }

  .btn-success {
    background-color: var(--success);
  }

  .btn-custom {
    background-color: var(--primary_00);
    color: var(--secondary_00);
  }

  .theme-references .color-references span {
    border-color: var(--primary_00);
  }

`