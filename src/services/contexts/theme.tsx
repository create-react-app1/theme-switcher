import React, { useState, createContext, ReactNode} from 'react'

interface ThemeProps {
  children?: ReactNode;
}

export interface ThemeProviderProps {
  theme: string;
  setTheme: Function;
}

export const ThemeContext: React.Context<Partial<ThemeProviderProps>> = createContext({})

const Theme: React.FC<ThemeProps> = ({ children }) => {

  const [theme, setTheme] = useState('main')

  return (
    <ThemeContext.Provider 
      value={{theme, setTheme}}
    >
      {children}
    </ThemeContext.Provider>
  )
}

export default Theme